msgid ""
msgstr ""
"Project-Id-Version: io.posidon.Paper main\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-19 17:20+0200\n"
"PO-Revision-Date: 2022-05-24 10:57-0300\n"
"Last-Translator: Juliano Dorneles dos Santos <juliano.dorneles@gmail.com>\n"
"Language-Team: Juliano Dorneles dos Santos <juliano.dorneles@gmail.com>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.0.1\n"

#: data/app.desktop.in:8 src/ui/strings.vala:3
msgid "Paper"
msgstr "Paper"

#: data/app.desktop.in:9
msgid "Notes Manager"
msgstr "Gerenciador de Notas"

#: data/app.desktop.in:10
msgid "Take notes"
msgstr "Tome notas"

#: data/app.desktop.in:12
msgid "Notebook;Note;Notes;Text;Markdown;Notepad;Write;School;Post-it;Sticky;"
msgstr ""
"Caderno;Nota;Notas;Texto;Markdown;Notepad;Escreva;Cole;Escola;Post-it;"
"Lembrete;"

#: data/app.desktop.in:21 src/ui/strings.vala:8
#: src/ui/popup/shortcut_window.blp:17 src/ui/window/window.blp:47
#: src/ui/window/window.blp:121
msgid "New Note"
msgstr "Nova Nota"

#: data/app.desktop.in:25 src/ui/strings.vala:14
#: src/ui/popup/shortcut_window.blp:37 src/ui/window/window.blp:146
msgid "New Notebook"
msgstr "Novo Caderno"

#: data/app.desktop.in:29 src/ui/strings.vala:39
#: src/ui/edit_view/toolbar/toolbar.blp:175 src/ui/window/window.blp:219
msgid "Markdown Cheatsheet"
msgstr "Dicas de Markdown"

#: data/app.desktop.in:33 src/ui/popup/shortcut_window.blp:62
msgid "Preferences"
msgstr "Preferências"

#: data/app.gschema.xml:6 src/ui/preferences/preferences.blp:15
msgid "Note Font"
msgstr "Fonte das Notas"

#: data/app.gschema.xml:7 src/ui/preferences/preferences.blp:16
msgid "The font notes will be displayed in"
msgstr "Fonte com a qual as notas serão exibidas"

#: data/app.gschema.xml:11 src/ui/preferences/preferences.blp:29
#, fuzzy
msgid "Monospace Font"
msgstr "Fonte das Notas"

#: data/app.gschema.xml:12 src/ui/preferences/preferences.blp:30
#, fuzzy
msgid "The font code will be displayed in"
msgstr "Fonte com a qual as notas serão exibidas"

#: data/app.gschema.xml:16 src/ui/preferences/preferences.blp:44
msgid "OLED Mode"
msgstr "Modo OLED"

#: data/app.gschema.xml:26
msgid "Notes Directory"
msgstr "Diretório de notas"

#: data/app.gschema.xml:27
msgid "Where the notebooks are stored"
msgstr "Onde as notas são armazenadas"

#: data/app.metainfo.xml.in:5
msgid "Take notes in Markdown"
msgstr "Tome notas em Markdown"

#: data/app.metainfo.xml.in:7
msgid "Create notebooks, take notes in markdown"
msgstr "Crie cadernos, tome notas em markdown"

#: data/app.metainfo.xml.in:8
msgid "Features:"
msgstr "Funcionalidades:"

#: data/app.metainfo.xml.in:10
msgid "Almost WYSIWYG markdown rendering"
msgstr "Renderização de markdown quase WYSIWYG"

#: data/app.metainfo.xml.in:11
msgid "Searchable through GNOME search"
msgstr "Pesquisável por meio da pesquisa do GNOME"

#: data/app.metainfo.xml.in:12
msgid "Highlight &amp; Strikethrough text formatting"
msgstr "Formato tachado e destacado"

#: data/app.metainfo.xml.in:13
msgid "App recoloring based on notebook color"
msgstr "Colorir o aplicativo com base na cor do caderno"

#: data/app.metainfo.xml.in:14
msgid "Trash can"
msgstr "Lixeira"

#: data/app.metainfo.xml.in:143
msgid "Markdown document"
msgstr "Documento markdown"

#: src/ui/strings.vala:4 src/ui/window/notebooks_bar/notebooks_bar.blp:125
msgid "Trash"
msgstr "Lixeira"

#: src/ui/strings.vala:5
#, c-format
msgid "%d Notes"
msgstr "%d Notas"

#: src/ui/strings.vala:6
msgid "Are you sure you want to delete everything in the trash?"
msgstr "Tem certeza de que deseja apagar tudo da lixeira?"

#: src/ui/strings.vala:7 src/ui/window/window.blp:54
msgid "Empty Trash"
msgstr "Esvaziar Lixeira"

#: src/ui/strings.vala:9
msgid "Create/choose a notebook before creating a note"
msgstr "Crie/escolha um caderno antes de criar uma nota"

#: src/ui/strings.vala:10
msgid "Please, select a note to edit"
msgstr "Por favor, selecione uma nota para editar"

#: src/ui/strings.vala:11
msgid "Please, select a note to delete"
msgstr "Por favor, selecione uma nota para apagar"

#: src/ui/strings.vala:12
msgid "Export Note"
msgstr "Exportar Nota"

#: src/ui/strings.vala:13
msgid "Export"
msgstr "Exportar"

#: src/ui/strings.vala:15
msgid "Rename Note"
msgstr "Renomear Nota"

#: src/ui/strings.vala:16
msgid "Move to Notebook"
msgstr "Mover para o Caderno"

#: src/ui/strings.vala:17
msgid "Move"
msgstr "Mover"

#: src/ui/strings.vala:18
#, c-format
msgid "Note “%s” already exists in notebook “%s”"
msgstr "Nota “%s” já existe no caderno “%s”"

#: src/ui/strings.vala:19
#, c-format
msgid "Are you sure you want to delete the note “%s”?"
msgstr "Tem certeza de que deseja apagar a nota “%s”?"

#: src/ui/strings.vala:20
msgid "Delete Note"
msgstr "Apagar Nota"

#: src/ui/strings.vala:21
#, c-format
msgid "Are you sure you want to delete the notebook “%s”?"
msgstr "Tem certeza de que deseja apagar o caderno “%s”?"

#: src/ui/strings.vala:22
msgid "Delete Notebook"
msgstr "Apagar Caderno"

#: src/ui/strings.vala:23 src/ui/popup/shortcut_window.blp:42
msgid "Edit Notebook"
msgstr "Editar Caderno"

#: src/ui/strings.vala:24
msgid "Note name shouldn’t contain “.” or “/”"
msgstr "O nome da nota não pode conter “.” o “/”"

#: src/ui/strings.vala:25
msgid "Note name shouldn’t be blank"
msgstr "O nome da nota não deve ficar em branco"

#: src/ui/strings.vala:26
#, c-format
msgid "Note “%s” already exists"
msgstr "Nota “%s” já existe"

#: src/ui/strings.vala:27
msgid "Couldn’t create note"
msgstr "Não foi possível criar a nota"

#: src/ui/strings.vala:28
msgid "Couldn’t change note"
msgstr "Não foi possível alterar a nota"

#: src/ui/strings.vala:29
msgid "Couldn’t delete note"
msgstr "Não foi possível apagar a nota"

#: src/ui/strings.vala:30
msgid "Couldn’t restore note"
msgstr "Não foi possível restaurar a nota"

#: src/ui/strings.vala:31
#, c-format
msgid "Saved “%s” to “%s”"
msgstr "“%s” salvo em “%s”"

#: src/ui/strings.vala:32
msgid "Unknown error"
msgstr "Erro desconhecido"

#: src/ui/strings.vala:33
msgid "Notebook name shouldn’t contain “.” or “/”"
msgstr "O nome do caderno não pode conter “.” o “/”"

#: src/ui/strings.vala:34
msgid "Notebook name shouldn’t be blank"
msgstr "O nome do caderno não deve ficar em branco"

#: src/ui/strings.vala:35
#, c-format
msgid "Notebook “%s” already exists"
msgstr "Caderno “%s” já existe"

#: src/ui/strings.vala:36
msgid "Couldn’t create notebook"
msgstr "Não foi possível criar o caderno"

#: src/ui/strings.vala:37
msgid "Couldn’t change notebook"
msgstr "Não foi possível alterar o caderno"

#: src/ui/strings.vala:38
msgid "Couldn’t delete notebook"
msgstr "Não foi possível excluir o caderno"

#: src/ui/strings.vala:40
msgid "Search"
msgstr "Buscar"

#: src/ui/strings.vala:41 src/ui/window/sidebar/note_menu.blp:36
msgid "Rename"
msgstr "Renomear"

#: src/ui/strings.vala:42
msgid "Couldn’t find an app to handle file uris"
msgstr ""
"Não foi possível encontrar um aplicativo para trabalhar com a uri do arquivo"

#: src/ui/strings.vala:43
msgid "Apply"
msgstr "Aplicar"

#: src/ui/strings.vala:44
#: src/ui/popup/notebook_selection_popup/notebook_selection_popup.blp:34
msgid "Cancel"
msgstr "Cancelar"

#: src/ui/strings.vala:45
msgid "Pick where the notebooks will be stored"
msgstr "Escolha onde os cadernos serão armazenados"

#: src/ui/strings.vala:46 src/ui/window/notebooks_bar/notebooks_bar.blp:80
msgid "All Notes"
msgstr "Todas as Notas"

#: src/ui/edit_view/toolbar/toolbar.blp:25
#: src/ui/edit_view/toolbar/toolbar.blp:116
#: src/ui/widgets/markdown/heading_popover.blp:48
msgid "Plain Text"
msgstr "Texto Simples"

#: src/ui/edit_view/toolbar/toolbar.blp:26
#: src/ui/edit_view/toolbar/toolbar.blp:117
#: src/ui/widgets/markdown/heading_popover.blp:12
msgid "Heading 1"
msgstr "Título 1"

#: src/ui/edit_view/toolbar/toolbar.blp:27
#: src/ui/edit_view/toolbar/toolbar.blp:118
#: src/ui/widgets/markdown/heading_popover.blp:18
msgid "Heading 2"
msgstr "Título 2"

#: src/ui/edit_view/toolbar/toolbar.blp:28
#: src/ui/edit_view/toolbar/toolbar.blp:119
#: src/ui/widgets/markdown/heading_popover.blp:24
msgid "Heading 3"
msgstr "Título 3"

#: src/ui/edit_view/toolbar/toolbar.blp:29
#: src/ui/edit_view/toolbar/toolbar.blp:120
#: src/ui/widgets/markdown/heading_popover.blp:30
msgid "Heading 4"
msgstr "Título 4"

#: src/ui/edit_view/toolbar/toolbar.blp:30
#: src/ui/edit_view/toolbar/toolbar.blp:121
#: src/ui/widgets/markdown/heading_popover.blp:36
msgid "Heading 5"
msgstr "Título 5"

#: src/ui/edit_view/toolbar/toolbar.blp:31
#: src/ui/edit_view/toolbar/toolbar.blp:122
#: src/ui/widgets/markdown/heading_popover.blp:42
msgid "Heading 6"
msgstr "Título 6"

#: src/ui/edit_view/toolbar/toolbar.blp:43
#: src/ui/edit_view/toolbar/toolbar.blp:133
#: src/ui/edit_view/toolbar/toolbar.blp:194 src/ui/popup/shortcut_window.blp:72
msgid "Bold"
msgstr "Negrito"

#: src/ui/edit_view/toolbar/toolbar.blp:50
#: src/ui/edit_view/toolbar/toolbar.blp:140
#: src/ui/edit_view/toolbar/toolbar.blp:195 src/ui/popup/shortcut_window.blp:77
msgid "Italic"
msgstr "Itálico"

#: src/ui/edit_view/toolbar/toolbar.blp:57
#: src/ui/edit_view/toolbar/toolbar.blp:147
#: src/ui/edit_view/toolbar/toolbar.blp:196 src/ui/popup/shortcut_window.blp:82
msgid "Strikethrough"
msgstr "Tachado"

#: src/ui/edit_view/toolbar/toolbar.blp:64
#: src/ui/edit_view/toolbar/toolbar.blp:154
#: src/ui/edit_view/toolbar/toolbar.blp:197 src/ui/popup/shortcut_window.blp:87
msgid "Highlight"
msgstr "Destacado"

#: src/ui/edit_view/toolbar/toolbar.blp:76
#: src/ui/edit_view/toolbar/toolbar.blp:186 src/ui/popup/shortcut_window.blp:92
msgid "Insert Link"
msgstr "Inserir Link"

#: src/ui/edit_view/toolbar/toolbar.blp:83
#: src/ui/edit_view/toolbar/toolbar.blp:187
msgid "Insert Code"
msgstr "Inserir Código"

#: src/ui/edit_view/toolbar/toolbar.blp:90
#: src/ui/edit_view/toolbar/toolbar.blp:188
msgid "Insert Horizontal Rule"
msgstr ""

#: src/ui/edit_view/toolbar/toolbar.blp:103
#, fuzzy
msgid "Format"
msgstr "Formato"

#: src/ui/edit_view/toolbar/toolbar.blp:167
#, fuzzy
msgid "Insert"
msgstr "Inserir Link"

#: src/ui/popup/notebook_selection_popup/notebook_selection_popup.blp:39
msgid "Confirm"
msgstr "Confirmar"

#: src/ui/popup/shortcut_window.blp:14
msgid "Note"
msgstr "Nota"

#: src/ui/popup/shortcut_window.blp:22
msgid "Edit Note"
msgstr "Editar Nota"

#: src/ui/popup/shortcut_window.blp:27
#, fuzzy
msgid "Save Note"
msgstr "Nova Nota"

#: src/ui/popup/shortcut_window.blp:34
msgid "Notebook"
msgstr "Caderno"

#: src/ui/popup/shortcut_window.blp:49
msgid "General"
msgstr "Geral"

#: src/ui/popup/shortcut_window.blp:52
msgid "Show Keyboard Shortcuts"
msgstr "Mostrar Atalhos de Teclado"

#: src/ui/popup/shortcut_window.blp:57
msgid "Quit"
msgstr "Sair"

#: src/ui/popup/shortcut_window.blp:69
msgid "Formatting"
msgstr "Formato"

#: src/ui/popup/shortcut_window.blp:99
msgid "Navigation"
msgstr "Navegação"

#: src/ui/popup/shortcut_window.blp:102 src/ui/window/window.blp:188
msgid "Toggle Sidebar"
msgstr "Alternar Barra Lateral"

#: src/ui/popup/shortcut_window.blp:107 src/ui/window/window.blp:63
msgid "Search Notes"
msgstr "Buscar Notas"

#: src/ui/preferences/preferences.blp:21
msgid "Pick a font for displaying the notes' content"
msgstr "Escolha uma fonte para exibir o conteúdo das notas"

#: src/ui/preferences/preferences.blp:35
#, fuzzy
msgid "Pick a font for displaying code"
msgstr "Escolha uma fonte para exibir o conteúdo das notas"

#: src/ui/preferences/preferences.blp:45
msgid "Makes the dark theme pitch black"
msgstr "Torna o tema escuro como preto"

#: src/ui/preferences/preferences.blp:55
msgid "Enable Toolbar"
msgstr ""

#: src/ui/preferences/preferences.blp:65
msgid "3-Pane Layout"
msgstr ""

#: src/ui/preferences/preferences.blp:77
msgid "Notes Storage Location"
msgstr "Local do armazenamento das notas"

#: src/ui/preferences/preferences.blp:78
msgid "Where the notebooks are stored (requires app restart)"
msgstr ""
"Onde os cadernos são armazenados (requer reinicialização do aplicativo)"

#: src/ui/widgets/theme_selector/theme_selector.blp:15
msgid "Follow system style"
msgstr ""

#: src/ui/widgets/theme_selector/theme_selector.blp:23
msgid "Light style"
msgstr ""

#: src/ui/widgets/theme_selector/theme_selector.blp:32
msgid "Dark style"
msgstr ""

#: src/ui/window/app_menu/app_menu.blp:24
msgid "_New Notebook"
msgstr "_Novo Caderno"

#: src/ui/window/app_menu/app_menu.blp:28
msgid "_Edit Notebook"
msgstr "_Editar Caderno"

#: src/ui/window/app_menu/app_menu.blp:34
msgid "_Preferences"
msgstr "_Preferências"

#: src/ui/window/app_menu/app_menu.blp:38
msgid "_Keyboard Shortcuts"
msgstr "_Teclas de Atalho"

#: src/ui/window/app_menu/app_menu.blp:42
msgid "_About"
msgstr "_Sobre o Paper"

#: src/ui/window/notebooks_bar/notebook_create_popup.blp:54
msgid "Notebook Name"
msgstr "Nome do Caderno"

#: src/ui/window/notebooks_bar/notebook_create_popup.blp:72
msgid "First Characters"
msgstr "Primeiros Caracteres"

#: src/ui/window/notebooks_bar/notebook_create_popup.blp:73
msgid "Initials"
msgstr "Iniciais"

#: src/ui/window/notebooks_bar/notebook_create_popup.blp:74
msgid "Initials: camelCase"
msgstr "Iniciais: camelCase"

#: src/ui/window/notebooks_bar/notebook_create_popup.blp:75
msgid "Initials: snake_case"
msgstr "Iniciais: snake_case"

#: src/ui/window/notebooks_bar/notebook_create_popup.blp:76
msgid "Icon"
msgstr "Ícone"

#: src/ui/window/notebooks_bar/notebook_create_popup.blp:106
msgid "Notebook Color"
msgstr "Cor do Caderno"

#: src/ui/window/notebooks_bar/notebook_create_popup.blp:116
msgid "Create Notebook"
msgstr "Criar Caderno"

#: src/ui/window/notebooks_bar/notebook_menu.blp:16
msgid "Edit"
msgstr "Editar"

#: src/ui/window/notebooks_bar/notebook_menu.blp:26
msgid "Move all to Trash"
msgstr "Mover Tudo para a Lixeira"

#: src/ui/window/sidebar/note_create_popup.blp:37
msgid "Note Name"
msgstr "Nome de Nota"

#: src/ui/window/sidebar/note_create_popup.blp:45
msgid "Create Note"
msgstr "Criar Nota"

#: src/ui/window/sidebar/note_menu.blp:16
msgid "Restore from Trash"
msgstr "Restaurar da Lixeira"

#: src/ui/window/sidebar/note_menu.blp:26
msgid "Delete from Trash"
msgstr "Apagar da Lixeira"

#: src/ui/window/sidebar/note_menu.blp:46
msgid "Move to Notebook…"
msgstr "Mover para o Caderno…"

#: src/ui/window/sidebar/note_menu.blp:56
msgid "Move to Trash"
msgstr "Mover para a Lixeira"

#: src/ui/window/sidebar/note_menu.blp:66
msgid "Open Containing Folder"
msgstr "Abrir Diretório de Armazenamento"

#: src/ui/window/window.blp:115
msgid "Get started writing"
msgstr "Comece a escrever"

#: src/ui/window/window.blp:140
msgid "Create a notebook"
msgstr "Cria um caderno"

#: src/ui/window/window.blp:165
msgid "Trash is empty"
msgstr "A lixeira está vazia"

#: src/ui/window/window.blp:196
msgid "Back"
msgstr ""

#: src/ui/window/window.blp:212
msgid "Open in Notebook"
msgstr "Abrir no Caderno"

#: src/ui/window/window.blp:233
msgid "_Rename Note"
msgstr "_Renomear Nota"

#: src/ui/window/window.blp:239
msgid "_Export Note"
msgstr "_Exportar Nota"

#~ msgid "Font"
#~ msgstr "Fonte"
